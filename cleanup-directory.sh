#!/bin/sh

keepweeklies=$((5 * 7))		#	minium number of latest weeklies to keep
keepdailies=13				# 	minium number of latest dailies to keep
keepmonthlies=$((12 * 28))	# 	minium number of latest monthlies to keep
minsize=0					#	minium size in bytes of a valid subdirectory or backup file
weeklies=testdir			#	directory of weekly backups
dailies=testdir-daily		#	directory of daily backups
monthlies=					#	directory of monthly backups
namescheme=backup*			#	name format of backup files or directories, replace non-recurring parts with placeholders for this to work

function check_this_dir { 
if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] ; then 
printf "faulty parameters. must be used like:\ncheck_this_dir [DIRECTORY TO CHECK] [MINIMUM ITEMS TO CHECK] [MINIMUM SIZE]\n"; exit 1; fi

dir=$1
retain=$2
size=$3

count=0
for file in $dir/$namescheme; do 
		if [ $(du -sL $file | awk '{print $1;}') -ge $size ]; then 
		count=$((count+1))
		fi
		if [ $count -ge $retain ]; then echo 'enough items available in' "$dir" ; return 0; fi
done
echo 'not enough items available in' "$dir" 
return 1
}

function cleanup_this_dir {
if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] ; then 
printf "faulty parameters. must be used like:\ncleanup_this_dir [DIRECTORY] [ITEMS TO KEEP] [MINIMUM SIZE]\n(OPTIONAL) [DIRECTORY TO CHECK AGAINST] [ITEMS TO KEEP] [MINIMUM SIZE]\nexample: cleanup_this_dir /backups/daily 7 4000 /backups/weekly 4 4000\n"; exit 1; fi

workdir=$1
workretain=$2
worksize=$3

checkdir=$4
checkretain=$5
checksize=$6

if [ $workdir == $checkdir ]||[ -z $checkdir ]; then 
	if check_this_dir $workdir $workretain $worksize; then 
	find $workdir/ -name "$namescheme" -type f -mtime +$workretain # -exec rm -rf {} \+
	return 0;
	else echo 'nothing to delete in' "$workdir"; return 1
	fi
else 
	if check_this_dir $workdir $workretain $worksize && check_this_dir $checkdir $checkretain $checksize; then
	find $workdir/ -name "$namescheme" -type f -mtime +$workretain # -exec rm -rf {} \+
	return 0;
	else echo 'nothing to delete in' "$workdir"; return 1
	fi
fi
}

cleanup_this_dir $dailies $keepdailies $minsize $weeklies $keepweeklies $minsize
