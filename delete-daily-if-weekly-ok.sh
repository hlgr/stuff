#!/bin/sh

retainweekly=1		#	number of weeklies to keep
retaindaily=3		# 	number of dailies to keep; set independently or $(($RETAINWEEKLY * 7 ))
minsize=7000		#	minium size in bytes of valid subdirectory or backup file
workdir=daily
checkdir=weekly

#aggregate list of weekly backup directories, based on timestamp in directory name, starting on given day of week i.e. day the weekly backup cronjob runs; exit if required number is not found
echo "---- weekly backups ----"
dir=()
week=1
while [ $week -le $retainweekly ] ; do 
dir=( ${dir[*]} $( find "$checkdir" -type d -name backup-"$( date -d "monday $week weeks ago" -I )" ) )
week=$(( week + 1 ))
done
if [ ${#dir[@]} -lt $retainweekly ] ; then echo "not enough recent backups found, exiting" && exit 1; else echo "enough recent backups found, checking size" ; fi
# if required number of weekly backups exist, check size; exit if any broken backups
status=0
for item in ${dir[*]}
do
	if [ $(du -sL $item | awk '{print $1;}') -ge $minsize ]; then status=$((status+0)) && echo "$item okay" ; else status=$((status+1)) && echo "$item broken"; fi  
done
if [ $status -ne 0 ]; then echo 'not enough valid recent backups, exiting' && exit 1; fi

#aggregate directory list of daily backup directories starting yesterday; allow tolerance of 2 days for missing intermittent backups 
echo "---- daily backups ----"
dir=()
day=1	# day=0 to include most recent backup
while [ $day -le $((retaindaily+2)) ] ; do 
dir=( ${dir[*]} $( find "$workdir" -type d -name backup-"$( date -d "$day days ago" -I )" ) )
day=$(( day + 1 ))
done
if [ ${#dir[@]} -lt $retaindaily ] ; then echo "not enough recent backups found, exiting" && exit 1; else echo "enough recent backups found, checking size" ; fi
# if required number of daily backups is found, check size and exit if any broken backups
status=0
for item in ${dir[*]}
do
	if [ $(du -sL $item | awk '{print $1;}') -ge $minsize ]; then status=$((status+0)) && echo "$item okay" ; else status=$((status+1)) && echo "$item broken"; fi  
done
if [ $status -ne 0 ]; then echo 'not enough valid recent backups, exiting' && exit 1; fi
# delete daily backups except the latest versions in the dir-array

echo "---- cleaning up ----"
dir=( $(printf "%s\n" "${dir[@]}" | sort -r | head -n+$retaindaily ) ) 
echo "all looks legit, deleting daily backups, retaining latest ${#dir[@]} versions"
delete=($(comm -3 <(printf "%s\n" "${dir[@]}" | sort) <(printf "$(find $workdir/ -type d -name "backup*")" | sort ) ))
echo rm -rf ${delete[*]}
