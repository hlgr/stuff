#/bin/bash

# based on https://gist.github.com/buchnema/da885a79593301b3d887f5655c77b991
# generates random list of german words from openthesaurus.de for use with keepassxc passphrase generator. openthesaurus data ist licensed under LGPL or CC-BY-SA 4.0.
# created wordlist files go here: on Linux, /usr/share/keepassxc/wordlists/, on macOS /Applications/KeePassXC.app/Contents/Resources and on Windows C:\Program Files\KeePassXC or C:\Program Files (x86)\KeePassXC 
# https://keepassxc.org/docs/#faq-general-wordlist
wget "https://www.openthesaurus.de/export/OpenThesaurus-Textversion.zip" && unzip OpenThesaurus-Textversion.zip && rm OpenThesaurus-Textversion.zip
grep -v '^#' openthesaurus.txt | \
tr ';' '\n' | \
tr "[:upper:]" "[:lower:]" | \
# remove vulgar expressions; remove words contained in blocklist.txt
sed '/vulg./d' | \
sed -e "$(sed 's:.*:/^&/Id:' blocklist.txt)" | \
# remove words containing umlauts or numbers
grep -v '[ÄÖÜäöü]' | \
grep -v '[^[:alnum:] _]' | \
grep -v '[0-9]' | \
grep -v ' ' | \
# limit list to words of certain length
grep -E '^.{4,9}$' | \
# remove duplicates; grab 10.000 random lines
sort -uR | \
head -n 10000 | \
# sort alphabetically and write to file
sort > de-openthesaurus-random.wordlist
rm openthesaurus.txt
