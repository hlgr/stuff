#!/bin/sh
function run_and_log {
	if [ ! -z "$logfile" ]; then
	#generate timestamp header and append to log file
	local timestamp=$(printf '%.0s\n' && now=$(date) && printf "%s\n" "$now" && printf "%0.s-" {1..30} && printf '%.0s\n')
	echo "$timestamp" >> $logfile
	"$@" &>>$logfile & wait $!
	local result=$?
	else  
	"$@" & wait $! 
	local result=$?
	fi
	#write short version of result to syslog
	if [ $result -eq 0 ]; then 
	echo "yay, $1 ran successfully" | systemd-cat -t "nextcloud"
	else echo "oh no, $1 failed with exit code $result" | systemd-cat -t "nextcloud" -p "err"
	fi
	return $result
}

#set to enable full output logging, could also bet set globally
logfile='/location/of/full/output.log'
#added --stats to rsync parameters for log output
run_and_log rsync --stats --archive --inplace --hard-links --delete-excluded --exclude="/*/files_trashbin/*" --exclude="/updater-*" --link-dest={{ nextcloud_data_backup_dir }}_`date -I -d yesterday` {{ nextcloud_data_dir }}/ {{ nextcloud_data_backup_dir }}_`date -I`/ 

jdupes -L -r {{ nextcloud_data_backup_dir }}_*
