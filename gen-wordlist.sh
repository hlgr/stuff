#/bin/bash

# based on https://gist.github.com/buchnema/da885a79593301b3d887f5655c77b991
# generates random list of german words from openthesaurus.de for use with keepassxc passphrase generator
wget "https://www.openthesaurus.de/export/OpenThesaurus-Textversion.zip" && unzip OpenThesaurus-Textversion.zip && rm OpenThesaurus-Textversion.zip
grep -v '^#' openthesaurus.txt | \
tr ';' '\n' | \
tr "[:upper:]" "[:lower:]" | \
tr "[ÄÖÜ]" "[äöü]" | \
grep  -v '[^[:alnum:] _]' | \
grep -v '[0-9]' | \
grep -v ' ' | \
# remove words with umlauts for easier input on non-german keyboards
grep -v '[äöü]' | \
grep -E '^.{4,8}$' | \
sort -uR | \
head -n 10000 | \
sort > de-openthesaurus-random.wordlist
rm LICENSE.txt openthesaurus.txt

